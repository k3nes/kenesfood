document.addEventListener('DOMContentLoaded', function () {
  const CONFIG = {
    DEBUG: false,
    ENDPOINTS: {
      session: '/order/session',
      list: '/order',
      order: '/order',
      cart: '/order/cart',
      countCart: '/order/countCart',
      productDetail: '/apis/product/detail/',
      done: '/order/done'
    },
    SELECTORS: {
      identityPage: '#identity-page',
      orderPage: '#order-page',
      customerName: '#customer-name',
      customerNameInput: '#inputCustomerName',
      submitNameBtn: '#submitName',
      categorySelect: '#category-select',
      showCartBtn: '#show-cart',
      cartCount: '#count-cart',
      cartModal: '#cart-modal',
      cartContainer: '#container-cart',
      productModal: '#detail-product',
      addToCartBtn: '#add-to-cart',
      orderBtn: '#order',
      productImageBtn: '.product-image-btn',
      viewProductBtn: '.view-product',
      removeCartItemBtn: '.remove-item'
    },
    MESSAGES: {
      errorLoadingProducts: 'Failed to load products. Please try again.',
      errorAddingToCart: 'Failed to add product',
      errorRemovingFromCart: 'Failed to remove product',
      cartEmptyMessage: 'Your cart is empty',
      successProductAdded: 'Product added to cart',
      successProductRemoved: 'Product removed from cart'
    }
  }

  // Utility Functions
  const Utils = {
    log (...args) {
      if (CONFIG.DEBUG) console.log('[OrderApp]', ...args)
    },
    error (...args) {
      console.error('[OrderApp Error]', ...args)
    },
    showLoading (message = 'Loading...') {
      return Swal.fire({
        title: message,
        didOpen: () => Swal.showLoading(),
        allowOutsideClick: false
      })
    },
    hideLoading () {
      Swal.close()
    },
    getUrlParams () {
      return {
        outletId: new URLSearchParams(window.location.search).get('outletId'),
        tableId: new URLSearchParams(window.location.search).get('tableId'),
        brand:
          new URLSearchParams(window.location.search).get('brand') || 'kopitiam'
      }
    }
  }

  // Order State Management
  const OrderState = {
    cart: [],
    isLoading: false,
    currentCategory: 'all',
    listeners: new Set(),

    setState (newState) {
      Object.assign(this, newState)
      this.notifyListeners()
    },
    subscribe (listener) {
      this.listeners.add(listener)
      return () => this.listeners.delete(listener)
    },
    notifyListeners () {
      this.listeners.forEach(listener => listener(this))
    }
  }

  // Cart Management
  class CartManager {
    static async loadCartContents () {
      if (OrderState.isLoading) return

      OrderState.setState({ isLoading: true })

      try {
        const params = Utils.getUrlParams()
        const response = await $.ajax({
          url: CONFIG.ENDPOINTS.cart,
          method: 'GET',
          data: params,
          timeout: 10000
        })

        if (response.success && response.data) {
          this.updateCartDisplay(response.data)
          const cartModal = new bootstrap.Modal(
            document.getElementById('cart-modal')
          )
          cartModal.show()
        } else {
          throw new Error(response.message || 'Failed to load cart')
        }
      } catch (error) {
        Swal.fire({
          title: 'Error',
          text: error.message,
          icon: 'error'
        })
      } finally {
        OrderState.setState({ isLoading: false })
      }
    }

    static updateCartDisplay (cartItems) {
      const cartContainer = document.querySelector(
        CONFIG.SELECTORS.cartContainer
      )
      if (!cartContainer) return

      cartContainer.innerHTML =
        cartItems.length === 0
          ? `<p>${CONFIG.MESSAGES.cartEmptyMessage}</p>`
          : this.createCartItemsHTML(cartItems)
    }

    static createCartItemsHTML (cartItems) {
      console.group('Cart Processing')
      console.log('Raw Cart Items:', cartItems)

      const extractPrice = item => {
        if (item.price_catalogue != null) {
          const price = parseFloat(item.price_catalogue)
          if (!isNaN(price)) {
            console.log(`Using price_catalogue: ${price}K`)
            return price
          }
        }

        if (item.product_price != null) {
          const price = parseFloat(item.product_price) / 1000
          if (!isNaN(price)) {
            console.log(`Using product_price: ${price}K`)
            return price
          }
        }

        console.warn(`No valid price found for ${item.product_name}`)
        return 0
      }

      const processedItems = cartItems.map(item => {
        const itemPrice = extractPrice(item)
        const itemQty = parseInt(item.quantity || 1)
        const itemSubtotal = itemPrice * itemQty

        return {
          ...item,
          processedPrice: itemPrice,
          processedQty: itemQty,
          processedSubtotal: itemSubtotal
        }
      })

      const totalPrice = processedItems.reduce(
        (acc, item) => acc + item.processedSubtotal,
        0
      )

      console.log('Processed Items:', processedItems)
      console.log('Total Price:', totalPrice)
      console.groupEnd()

      // Header section
      const header = `
          <div class="cart-header bg-white text-white p-4 rounded-top">
              <h4 class="mb-0">Keranjang Belanja</h4>
              <p class="mb-0 mt-1 text-sm opacity-90">${processedItems.length} item</p>
          </div>
      `

      // Items section
      const items = processedItems
        .map(
          item => `
          <div class="cart-item border-bottom p-4">
              <div class="d-flex align-items-start">
                  <!-- Product Image -->
                  <div class="cart-item-image me-4">
                      <img src="${
                        window.location.origin
                      }/resource/assets-frontend/dist/product/${
            item.product_pict
          }"
                           alt="${item.product_name}"
                           class="rounded shadow-sm"
                           style="width: 80px; height: 80px; object-fit: cover;">
                  </div>
                  
                  <!-- Product Details -->
                  <div class="cart-item-details flex-grow-1">
                      <div class="d-flex justify-content-between align-items-start mb-2">
                          <h5 class="mb-0 text-primary">${
                            item.product_name
                          }</h5>
                          <button class="btn btn-icon btn-sm btn-outline-danger rounded-circle remove-item" 
                                  data-product-id="${item.product_id}">
                              <i class="fa fa-times"></i>
                          </button>
                      </div>
                      
                      <div class="d-flex justify-content-between align-items-end">
                          <div class="quantity-controls">
                              <div class="d-flex align-items-center">
                                  <button class="btn btn-sm btn-outline-secondary decrease-qty me-2"
                                          data-product-id="${
                                            item.product_id
                                          }">-</button>
                                  <span class="qty-display">${
                                    item.processedQty
                                  }</span>
                                  <button class="btn btn-sm btn-outline-secondary increase-qty ms-2"
                                          data-product-id="${item.product_id}"
                                          ${
                                            item.product_stock <=
                                            item.processedQty
                                              ? 'disabled'
                                              : ''
                                          }>+</button>
                              </div>
                              <small class="text-muted mt-1">Stok: ${
                                item.product_stock
                              }</small>
                          </div>
                          <div class="price-info text-end">
                              <h6 class="mb-1">Rp ${item.processedPrice.toLocaleString(
                                'id-ID'
                              )}K</h6>
                              <p class="subtotal mb-0 text-primary">
                                  Subtotal: Rp ${item.processedSubtotal.toLocaleString(
                                    'id-ID'
                                  )}K
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      `
        )
        .join('')

      // Footer section with total
      const footer = `
          <div class="cart-footer p-4 bg-light rounded-bottom">
              <div class="row align-items-center">
                  <div class="col-6">
                      <h5 class="mb-0">Total Pembayaran:</h5>
                  </div>
                  <div class="col-6 text-end">
                      <h4 class="mb-0 text-primary">Rp ${totalPrice.toLocaleString(
                        'id-ID'
                      )}K</h4>
                  </div>
              </div>
      `

      // Empty cart state
      if (processedItems.length === 0) {
        return `
              <div class="text-center py-5">
                  <i class="fas fa-shopping-cart fa-3x text-muted mb-3"></i>
                  <h5 class="text-muted">Keranjang Belanja Kosong</h5>
                  <p class="text-muted mb-4">Silakan tambahkan produk ke keranjang</p>
                  <button class="btn btn-primary" data-bs-dismiss="modal">
                      Lanjutkan Belanja
                  </button>
              </div>
          `
      }

      // Combine all sections
      return `
          <div class="cart-container shadow-sm">
              ${header}
              <div class="cart-items">
                  ${items}
              </div>
              ${footer}
          </div>
      `
    }

    static async removeCartItem (productId) {
      try {
        if (!productId || isNaN(productId)) {
          throw new Error('Invalid Product ID')
        }

        const params = Utils.getUrlParams()
        const response = await $.ajax({
          type: 'DELETE',
          url: CONFIG.ENDPOINTS.order,
          data: JSON.stringify({
            ...params,
            productId: parseInt(productId, 10)
          }),
          contentType: 'application/json',
          dataType: 'json'
        })

        if (!response.success) {
          throw new Error(response.message || 'Failed to remove product')
        }

        await this.loadCartContents()
        await startCartCounter()

        Swal.fire({
          title: 'Berhasil',
          text: 'Produk telah dihapus dari keranjang',
          icon: 'success',
          timer: 1500,
          showConfirmButton: false
        })
      } catch (error) {
        Swal.fire({
          title: 'Kesalahan',
          text: error.message || CONFIG.MESSAGES.errorRemovingFromCart,
          icon: 'error'
        })
      }
    }

    static async loadCartContents () {
      if (OrderState.isLoading) return

      OrderState.setState({ isLoading: true })

      try {
        const params = Utils.getUrlParams()
        const response = await $.ajax({
          url: CONFIG.ENDPOINTS.cart,
          method: 'GET',
          data: params,
          timeout: 10000
        })

        if (response.success && response.data) {
          CartManager.updateCartDisplay(response.data)
          const cartModal = new bootstrap.Modal(
            document.getElementById('cart-modal')
          )
          cartModal.show()
        } else {
          throw new Error(response.message || 'Failed to load cart')
        }
      } catch (error) {
        Swal.fire({
          title: 'Error',
          text: error.message,
          icon: 'error'
        })
      } finally {
        OrderState.setState({ isLoading: false })
      }
    }

    static async updateQuantity (productId, action) {
      try {
        const params = Utils.getUrlParams()
        const response = await $.ajax({
          type: 'POST',
          url: CONFIG.ENDPOINTS.order,
          data: JSON.stringify({
            ...params,
            productId: productId,
            action: action
          }),
          contentType: 'application/json',
          dataType: 'json'
        })

        console.log('Update Quantity Response:', response)

        if (!response.success) {
          return Swal.fire({
            title: 'Gagal',
            text: response.message || 'Gagal mengupdate kuantitas',
            icon: 'error'
          })
        }

        // Manually close the modal
        const cartModal = bootstrap.Modal.getInstance(
          document.getElementById('cart-modal')
        )
        if (cartModal) {
          cartModal.hide()
        }

        // Remove modal backdrop manually
        $('.modal-backdrop').remove()
        $('body').removeClass('modal-open')

        // Reload cart contents
        await this.loadCartContents()
        await startCartCounter()

        Swal.fire({
          title: 'Berhasil',
          text: 'Kuantitas berhasil diperbarui',
          icon: 'success',
          timer: 1500,
          showConfirmButton: false
        })
      } catch (error) {
        console.error('Update Quantity Detailed Error:', error)

        // Manually close the modal if an error occurs
        const cartModal = bootstrap.Modal.getInstance(
          document.getElementById('cart-modal')
        )
        if (cartModal) {
          cartModal.hide()
        }
        $('.modal-backdrop').remove()
        $('body').removeClass('modal-open')

        Swal.fire({
          title: 'Error',
          text:
            error.responseJSON?.message ||
            error.message ||
            'Gagal mengupdate kuantitas',
          icon: 'error'
        })
      }
    }
  }

  // Product Management
  class ProductManager {
    static createProductElement (product) {
      const productCol = document.createElement('div')
      productCol.className = 'col-6 col-sm-4 pt-3 product-image-btn'
      productCol.setAttribute('data-product-id', product.product_id)
      productCol.setAttribute('data-category-id', product.cat_id)

      productCol.innerHTML = `
        <div class="position-relative mb-3">
          <div class="product-image-container">
            <img src="${
              window.location.origin
            }/resource/assets-frontend/dist/product/${product.product_pict}"
                 alt="${product.product_name}"
                 class="img-fluid rounded product-image"
                 style="max-height: 200px; object-fit: cover;">
          </div>
          <div class="product-info p-2">
            <h5 class="product-name fw-bold mb-2">
              ${product.product_name.toUpperCase()}
            </h5>
            <div class="price-tag mb-3">
              <span class="rounded-circle custom-catalogue-dotrp">Rp</span>
              <span class="custom-catalogue-text">
                ${product.price_catalogue} <sup>K</sup>
              </span>
            </div>
            <button class="btn btn-primary view-product w-100" 
                    ${product.stock == 0 ? 'disabled' : ''}>
              Pilih
            </button>
          </div>
        </div>
      `

      return productCol
    }

    static async loadProductDetails (productId) {
      try {
        const response = await $.ajax({
          type: 'GET',
          url: `${window.location.origin}${CONFIG.ENDPOINTS.productDetail}${productId}`,
          dataType: 'json'
        })

        if (response.data && response.data.detail) {
          this.updateProductModal(response.data.detail)
          return response.data.detail
        }
      } catch (error) {
        Swal.fire({
          title: 'Error',
          text: 'Failed to load product details',
          icon: 'error'
        })
      }
    }

    static updateProductModal (detail) {
      $('#detail-product-label').text(detail.product_name)
      $('#detail-product-description').text(
        detail.product_desc || 'No description available'
      )
      $('#detail-product-image').attr(
        'src',
        `${window.location.origin}/resource/assets-frontend/dist/product/${detail.product_pict}`
      )
      $('#product-id').text(detail.product_id)

      const stockAvailable = parseInt(detail.stock) > 0
      $('#add-to-cart').toggle(stockAvailable).prop('disabled', !stockAvailable)

      const productModal = new bootstrap.Modal(
        document.getElementById('detail-product')
      )
      productModal.show()

      $('#detail-product').on('hidden.bs.modal', function () {
        $('.modal-backdrop').remove()
        $('body').removeClass('modal-open')
        $(this).modal('dispose')
        $(this).off('hidden.bs.modal')
      })
    }

    static async addToCart (productId) {
      try {
        const params = Utils.getUrlParams()
        console.log('Add to Cart Params:', params)
        console.log('Product ID:', productId)

        const payload = {
          ...params,
          productId: productId,
          action: 'addProduct'
        }

        console.log('Complete Payload:', payload)

        const response = await $.ajax({
          type: 'POST',
          url: CONFIG.ENDPOINTS.order,
          data: JSON.stringify(payload),
          contentType: 'application/json',
          dataType: 'json'
        })

        console.log('Add to Cart Full Response:', response)

        if (!response.success) {
          return Swal.fire({
            title: 'Gagal',
            text: response.message || 'Gagal menambahkan produk',
            icon: 'error'
          })
        }

        $('#detail-product').modal('hide')
        await startCartCounter()
        await CartManager.loadCartContents()

        Swal.fire({
          title: 'Berhasil',
          text: CONFIG.MESSAGES.successProductAdded,
          icon: 'success',
          timer: 1500,
          showConfirmButton: false
        })
      } catch (error) {
        console.error('Add to Cart Detailed Error:', error)

        if (error.responseJSON) {
          console.error('Error Response JSON:', error.responseJSON)

          Swal.fire({
            title: 'Error',
            text: error.responseJSON.message || 'Gagal menambahkan produk',
            icon: 'error'
          })
        } else {
          Swal.fire({
            title: 'Error',
            text: error.message || CONFIG.MESSAGES.errorAddingToCart,
            icon: 'error'
          })
        }
      }
    }
  }

  // Category Management
  class CategoryManager {
    static async filterProducts (selectedCategory) {
      try {
        const params = Utils.getUrlParams()

        if (selectedCategory !== 'all') {
          params.category = selectedCategory
        }

        Utils.showLoading()

        const response = await $.ajax({
          type: 'GET',
          url: `${window.location.origin}${CONFIG.ENDPOINTS.list}`,
          data: params,
          dataType: 'json',
          timeout: 10000
        })

        Utils.hideLoading()

        if (response.success && response.data?.grouped_products) {
          this.updateProductDisplay(
            selectedCategory,
            response.data.grouped_products
          )
          this.updateCategoryURL(selectedCategory)
        } else {
          throw new Error(
            response.message || CONFIG.MESSAGES.errorLoadingProducts
          )
        }
      } catch (error) {
        Utils.hideLoading()
        Swal.fire({
          title: 'Error',
          text: error.message,
          icon: 'error'
        })
      }
    }

    static updateProductDisplay (selectedCategory, groupedProducts) {
      const container = document.querySelector(
        '.container .row.text-center[style="margin-left: 0;"]'
      )
      if (!container) return

      container.innerHTML = ''

      Object.entries(groupedProducts).forEach(([categoryId, category]) => {
        if (selectedCategory === 'all' || categoryId === selectedCategory) {
          const categorySection = this.createCategorySection(category)
          container.appendChild(categorySection)
        }
      })
    }

    static createCategorySection (category) {
      const categorySection = document.createElement('div')
      categorySection.className = 'row text-center align-items-end'
      categorySection.style.marginLeft = '0'

      const headerCol = document.createElement('div')
      headerCol.className = 'col-12 pt-2 rounded'
      headerCol.style.backgroundColor = '#62411e'
      headerCol.innerHTML = `
        <div class="d-flex justify-content-center">
          <h2 style="color: #fff">${category.category_name}</h2>
        </div>
      `
      categorySection.appendChild(headerCol)

      category.products.forEach(product => {
        const productCol = ProductManager.createProductElement(product)
        categorySection.appendChild(productCol)
      })

      return categorySection
    }

    static updateCategoryURL (selectedCategory) {
      const newParams = new URLSearchParams(window.location.search)

      if (selectedCategory !== 'all') {
        newParams.set('category', selectedCategory)
      } else {
        newParams.delete('category')
      }

      window.history.replaceState({}, '', `?${newParams.toString()}`)
    }
  }

  // Event Binding
  function setupEventListeners () {
    // Category Selection
    $(document).on('change', CONFIG.SELECTORS.categorySelect, function () {
      const selectedCategory = $(this).val()
      CategoryManager.filterProducts(selectedCategory)
    })

    // Product Modal Trigger
    $(document).on(
      'click',
      `${CONFIG.SELECTORS.productImageBtn}, ${CONFIG.SELECTORS.viewProductBtn}`,
      async function (e) {
        e.preventDefault()
        const productId = $(this)
          .closest(CONFIG.SELECTORS.productImageBtn)
          .data('product-id')
        await ProductManager.loadProductDetails(productId)
      }
    )

    // Add to Cart
    $(document).on('click', CONFIG.SELECTORS.addToCartBtn, async function () {
      const productId = $('#product-id').text()
      await ProductManager.addToCart(productId)
    })

	$(document).on('hidden.bs.modal', '#cart-modal', function () {
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open');
	});

    // Cart Interactions
    $(document).on(
      'click',
      CONFIG.SELECTORS.showCartBtn,
      CartManager.loadCartContents
    )
    $(document).on('click', CONFIG.SELECTORS.removeCartItemBtn, function () {
      const productId = $(this).data('product-id')
      CartManager.removeCartItem(productId)
    })

    $(document).on('click', '.increase-qty', function () {
      const productId = $(this).data('product-id')
      CartManager.updateQuantity(productId, 'increase')
    })

    $(document).on('click', '.decrease-qty', function () {
      const productId = $(this).data('product-id')
      CartManager.updateQuantity(productId, 'decrease')
    })

    // Order Submission
    $(document).on('click', CONFIG.SELECTORS.orderBtn, async function () {
      try {
        const orderValidation = await validateOrderTotal()
        console.log('Order Validation:', orderValidation)

        if (!orderValidation.valid) {
          return Swal.fire({
            title: 'Keranjang Kosong',
            text: 'Silakan tambahkan produk terlebih dahulu',
            icon: 'warning'
          })
        }

        const params = Utils.getUrlParams()
        const response = await $.ajax({
          type: 'POST',
          url: CONFIG.ENDPOINTS.done,
          data: JSON.stringify(params),
          contentType: 'application/json',
          dataType: 'json'
        })

        console.log('Order Response:', response)

        if (response.success) {
          Swal.fire({
            title: 'Pesanan Berhasil',
            html: `
                      <p>Total Pesanan: Rp ${orderValidation.total.toLocaleString(
                        'id-ID'
                      )}K</p>
                      <p>Status: Pesanan Selesai</p>
                  `,
            icon: 'success',
            showConfirmButton: true,
            confirmButtonText: 'Tutup'
          }).then(() => {
            // Hapus data sesi
            localStorage.removeItem('processedSessions')

            $('#cart-modal').modal('hide')

            // Reset halaman
            setTimeout(() => {
              window.location.reload()
            }, 500)
          })
        } else {
          throw new Error(response.message || 'Gagal memproses pesanan')
        }
      } catch (error) {
        console.error('Order Error:', error)
        Swal.fire({
          title: 'Error',
          text: error.responseJSON?.message || 'Gagal memproses pesanan',
          icon: 'error'
        })
      }
    })
  }

  // Cart Counter
  async function startCartCounter () {
    try {
      const params = Utils.getUrlParams()
      const response = await $.ajax({
        type: 'GET',
        url: CONFIG.ENDPOINTS.countCart,
        data: params,
        dataType: 'json'
      })

      $(CONFIG.SELECTORS.cartCount).text(response.data || 0)
    } catch (error) {
      console.error('Cart Counter Error', error)
    }
  }

  async function validateOrderTotal () {
    try {
      const params = Utils.getUrlParams()
      const response = await $.ajax({
        type: 'GET',
        url: CONFIG.ENDPOINTS.cart,
        data: params,
        dataType: 'json',
        timeout: 30000
      })

      if (!response.success) {
        throw new Error(response.message || 'Failed to validate order')
      }

      if (response.success && response.data && response.data.length > 0) {
        const processedItems = response.data.map(item => {
          const extractPrice = () => {
            const prices = [item.price_catalogue, item.product_price]

            for (let price of prices) {
              if (price !== undefined && price !== null) {
                const parsedPrice = parseFloat(price)
                return parsedPrice / 1000
              }
            }

            console.warn(`No valid price found for ${item.product_name}`)
            return 0
          }

          const itemPrice = extractPrice()
          const itemQty = parseInt(item.quantity || item.product_count || 1)
          const itemSubtotal = itemPrice * itemQty

          console.log(`Item Processing:
                    Name: ${item.product_name}
                    Price: ${itemPrice}
                    Quantity: ${itemQty}
                    Subtotal: ${itemSubtotal}`)

          return {
            ...item,
            processedPrice: itemPrice,
            processedQty: itemQty,
            processedSubtotal: itemSubtotal
          }
        })

        const total = processedItems.reduce(
          (acc, item) => acc + item.processedSubtotal,
          0
        )

        console.log('Processed Items for Total:', processedItems)
        console.log('Calculated Total:', total)

        return {
          valid: total > 0,
          total: total
        }
      }

      return { valid: false, total: 0 }
    } catch (error) {
      console.error('Order Total Validation Error:', error)
      return { valid: false, total: 0 }
    }
  }

  // Initialize
  function init () {
    setupEventListeners()
    checkCurrentSession()
    startCartCounter()
  }

  // Session Management
  function checkCurrentSession () {
    $.ajax({
      type: 'GET',
      url: `${window.location.origin}${
        CONFIG.ENDPOINTS.session
      }?${new URLSearchParams(window.location.search).toString()}`,
      dataType: 'json',
      timeout: 10000
    })
      .done(function (response) {
        if (response.data) {
          $(CONFIG.SELECTORS.customerName).text(response.data.name)
          $(CONFIG.SELECTORS.identityPage).attr('hidden', true)
          $(CONFIG.SELECTORS.orderPage).removeAttr('hidden')
          startCartCounter()
        } else {
          $(CONFIG.SELECTORS.identityPage).removeAttr('hidden')
          $(CONFIG.SELECTORS.orderPage).attr('hidden', true)
        }
      })
      .fail(function (xhr) {
        Swal.fire({
          title: 'Error',
          text: xhr.responseJSON?.message || 'Gagal memeriksa sesi',
          icon: 'error'
        })
      })
  }

  // Customer Name Submission
  function submitCustomerName () {
    const name = $(CONFIG.SELECTORS.customerNameInput).val().trim()

    if (!name) {
      Swal.fire({
        title: 'Error',
        text: 'Nama tidak boleh kosong',
        icon: 'error'
      })
      return
    }

    const params = Utils.getUrlParams()

    $.ajax({
      type: 'POST',
      url: `${window.location.origin}${CONFIG.ENDPOINTS.session}`,
      data: JSON.stringify({
        ...params,
        name: name
      }),
      contentType: 'application/json',
      dataType: 'json'
    })
      .done(function (response) {
        const sessionData = response.data

        Swal.fire({
          title: 'Mohon Tunggu Sebentar',
          html: 'Waiter Sedang Menuju Kemeja Anda...<br>Anda akan dialihkan ke Daftar Menu dalam <b></b> detik.',
          timer: 8000,
          timerProgressBar: true,
          didOpen: () => {
            Swal.showLoading()
            const timer = Swal.getPopup().querySelector('b')
            const timerInterval = setInterval(() => {
              timer.textContent = `${Math.ceil(Swal.getTimerLeft() / 1000)}`
            }, 100)
          },
          didClose: () => {
            $(CONFIG.SELECTORS.customerName).text(sessionData.name)
            $(CONFIG.SELECTORS.identityPage).attr('hidden', true)
            $(CONFIG.SELECTORS.orderPage).removeAttr('hidden')
            startCartCounter()

            window.location.href = "ekatalog/?brand=kopitiam&type=outlet";
          }
        })
      })
      .fail(function (xhr) {
        Swal.fire({
          title: 'Error',
          text: xhr.responseJSON?.message || 'Gagal membuat sesi',
          icon: 'error'
        })
      })
  }

  // Event Binding for Name Submission
  $(CONFIG.SELECTORS.submitNameBtn).on('click', submitCustomerName)
  $(CONFIG.SELECTORS.customerNameInput).on('keypress', function (event) {
    if (event.which === 13) {
      event.preventDefault()
      submitCustomerName()
    }
  })

  // Initialize the application
  init()
})
