// Global Variables
var statusTable = []
var openDetailSign = ['1']
var baseUrl
var outletParams
var approvalModal
var orderModal

// Status Management
function changeTableStatus (tableId, status) {
  var elem = $('#tableStatus-' + tableId)
  var statusClass = 'badge-info'
  var statusText = 'Kosong'

  // Reset classes
  elem.removeClass('badge-info badge-success badge-warning')

  // Determine new status
  switch (parseInt(status)) {
    case 2: // Waiting
      statusClass = 'badge-warning'
      statusText = 'Menunggu'
      break
    case 1: // Reserved
      statusClass = 'badge-success'
      statusText = 'Terisi'
      break
    case 0: // Empty
    default:
      statusClass = 'badge-info'
      statusText = 'Kosong'
      break
  }

  // Apply new status
  elem.addClass(statusClass).text(statusText)
}

// Session Management
function getProcessedSessions () {
  var stored = localStorage.getItem('processedSessions')
  return stored ? JSON.parse(stored) : []
}

function setProcessedSession (sessionId) {
  var processed = getProcessedSessions()
  if (processed.indexOf(sessionId) === -1) {
    processed.push(sessionId)
    localStorage.setItem('processedSessions', JSON.stringify(processed))
  }
}

function isProcessedSession (sessionId) {
  return getProcessedSessions().indexOf(sessionId) > -1
}

// Audio Controls
function playNotification () {
  var bell = document.getElementById('orderBell')
  if (bell && !bell.playing) {
    bell.play()
    bell.loop = true
  }
}

function stopNotification () {
  var bell = document.getElementById('orderBell')
  if (bell) {
    bell.pause()
    bell.currentTime = 0
    bell.loop = false
  }
}

// API Functions
function fetchWaitingSessions () {
  $.ajax({
    type: 'GET',
    url:
      baseUrl +
      '/getData?action=getWaitingApprovalSessions&' +
      outletParams.toString(),
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
      if (response.success && response.data && response.data.length > 0) {
        handleWaitingSessions(response.data)
      }
    },
    error: handleAjaxError
  })
}

function updateTableStatuses () {
  $.ajax({
    type: 'GET',
    url: baseUrl + '/getData?action=getStatusTable&' + outletParams.toString(),
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
      if (response.success && response.data) {
        for (var i = 0; i < response.data.length; i++) {
          changeTableStatus(i + 1, response.data[i])
        }
      }
    },
    error: handleAjaxError
  })
}

function approveSession (session) {
  $.ajax({
    type: 'POST',
    url: window.location.origin + '/order/approve',
    data: JSON.stringify({
      outletId: session.outlet_id,
      tableId: session.table_id,
      brand: session.brand
    }),
    contentType: 'application/json',
    dataType: 'json',
    success: function () {
      setProcessedSession(session.id)
      approvalModal.hide()
      stopNotification()
      showSuccessMessage('Sesi pelanggan disetujui')
      setTimeout(updateTableStatuses, 1000)
    },
    error: handleAjaxError
  })
}

// function rejectSession (session) {
//   $.ajax({
//     type: 'POST',
//     url: window.location.origin + '/order/reject',
//     data: JSON.stringify({
//       outletId: session.outlet_id,
//       tableId: session.table_id,
//       brand: session.brand
//     }),
//     contentType: 'application/json',
//     dataType: 'json',
//     success: function () {
//       setProcessedSession(session.id)
//       approvalModal.hide()
//       stopNotification()
//       showSuccessMessage('Sesi pelanggan ditolak')
//     },
//     error: handleAjaxError
//   })
// }

// Event Handlers
function handleWaitingSessions (sessions) {
  for (var i = 0; i < sessions.length; i++) {
    if (!isProcessedSession(sessions[i].id)) {
      showApprovalModal(sessions[i])
    }
  }
}

function showApprovalModal (session) {
  $('#approval-customer-name').text(session.name || '')
  $('#approval-table-number').text(session.table_id || '')
  $('#approval-outlet-name').text(outletParams.get('outletId') || '')
  $('#approvalModal').data('session', session)
  playNotification()
  approvalModal.show()
}

function handleAjaxError (xhr) {
  console.error('Ajax Error: ' + xhr.responseText)
  var message = 'Terjadi kesalahan'
  if (xhr.responseJSON && xhr.responseJSON.message) {
    message = xhr.responseJSON.message
  }
  showErrorMessage(message)
}

// UI Helpers
function showSuccessMessage (message) {
  Swal.fire({
    title: 'Berhasil',
    text: message,
    icon: 'success'
  })
}

function showErrorMessage (message) {
  Swal.fire({
    title: 'Error',
    text: message,
    icon: 'error'
  })
}

// Initialization
document.addEventListener('DOMContentLoaded', function () {
  // Initialize Variables
  var url = new URL(window.location.href)
  baseUrl =
    url.protocol +
    '//' +
    url.host +
    url.pathname.split('/').slice(0, 4).join('/')
  outletParams = new URLSearchParams()
  outletParams.append('outletId', url.pathname.split('/').pop())
  outletParams.append('brand', url.searchParams.get('brand'))

  // Initialize Modals
  approvalModal = new bootstrap.Modal('#approvalModal')
  orderModal = new bootstrap.Modal('#detailOrder')

  // Event Listeners
  $('#approve-session').on('click', function () {
    var session = $('#approvalModal').data('session')
    if (session) {
      approveSession(session)
    }
  })

//   $('#reject-session').on('click', function () {
//     var session = $('#approvalModal').data('session')
//     if (session) {
//       rejectSession(session)
//     }
//   })

  $('#approvalModal').on('hidden.bs.modal', stopNotification)

  // Start Polling
  setInterval(fetchWaitingSessions, 5000)
  setInterval(updateTableStatuses, 4000)

  // Initial Table Status Update
  updateTableStatuses()

  // Cleanup
  window.addEventListener('beforeunload', stopNotification)
})
