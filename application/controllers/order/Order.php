<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

require_once(APPPATH . 'controllers/base/PublicBase.php');

/**
 * Public Order Controller Class
 * 
 * Handles all public order-related operations and views
 */
class Order extends ApplicationBase
{
	/**
	 * @var string Table name for orders
	 */
	private const TABLE = 'orders';
	/**
	 * @var string Default brand type
	 */
	private const DEFAULT_BRAND = 'kopitiam';

	/**
	 * @var int Default session expiry time in minutes
	 */
	private const DEFAULT_SESSION_EXPIRY = 1;

	/**
	 * @var int Extended session expiry time in hours
	 */
	private const EXTENDED_SESSION_EXPIRY = 1;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->loadRequiredModels();
	}

	/**
	 * Load all required models
	 */
	private function loadRequiredModels(): void
	{
		$this->load->model('M_categories');
		$this->load->model('order/M_Order', 'M_Order');
		$this->load->model('M_products');
		$this->load->model('order/cashier/M_Product', 'MOC_Product');
		$this->load->model('order/cashier/M_Order_Detail', 'MOC_Order_Detail');
		$this->load->model("settings/M_outlets", "MS_Outlet");
	}

	/**
	 * Get session status
	 */
	public function session()
	{
		try {
			$params = $this->getAndValidateInputParams();

			$this->logDebug('Session Request Detailed Input: ' . $this->formatParams($params));

			$this->M_Order->cleanExpiredAndRejectedSessions($params['outlet_id'], $params['brand']);

			$data = $this->M_Order->getOne(array_merge($params, [
				'status' => 0,
				'deleted_at' => NULL
			]));

			$this->logDebug('Session Detailed Final Result: ' . print_r($data, true));

			return $this->sendSuccessResponse(
				$data ? 'Sesi disetujui' : 'Tidak ada sesi aktif',
				$data
			);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Create new session
	 */
	public function createSession()
	{
		try {
			$payload = $this->getValidatedPayload();

			$existingSession = $this->M_Order->getOne([
				'outlet_id' => $payload->outletId,
				'table_id' => $payload->tableId,
				'brand' => $payload->brand,
				'deleted_at' => NULL,
				'status' => $this->M_Order::STATUS_RESERVED
			]);

			if ($existingSession) {
				return $this->sendSuccessResponse(
					'Menunggu persetujuan kasir',
					$existingSession
				);
			}

			$result = $this->M_Order->insertOrder($this->prepareNewSessionData($payload));

			return $this->sendSuccessResponse(
				'Menunggu persetujuan kasir',
				$result['data']
			);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Approve session
	 */
	public function approveSession()
	{
		try {
			$payload = json_decode($this->input->raw_input_stream);
			$order = $this->findWaitingOrder($payload);

			if (!$order) {
				throw new Exception('Sesi tidak ditemukan');
			}

			$currentTime = date('Y-m-d H:i:s');
			$this->M_Order->qBUpdate($order['id'], [
				'status' => $this->M_Order::STATUS_RESERVED,
				'updated_at' => $currentTime,
				'expire_at' => date('Y-m-d H:i:s', strtotime('+1 minute'))
			]);

			return $this->sendSuccessResponse('Sesi disetujui', [
				'approved_at' => $currentTime
			]);
		} catch (Exception $e) {
			$this->logError('Approve Session Error: ' . $e->getMessage());
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Get waiting approval sessions
	 * 
	 * @return array List of waiting sessions
	 */
	private function getWaitingApprovalSessions(): array
	{
		try {
			$outletId = $this->input->get('outletId');
			$brand = $this->input->get('brand');

			$sessions = $this->db->select('*')
				->from('orders')
				->where([
					'outlet_id' => $outletId,
					'brand' => $brand,
					'deleted_at' => NULL,
					'status' => $this->M_Order::STATUS_RESERVED
				])
				->where('TIMESTAMPDIFF(MINUTE, created_at, NOW()) <', 1)
				->where('expire_at >', $this->getCurrentDateTime())
				->get()
				->result_array();

			return array_map(function ($session) {
				$session['is_new'] = true;
				return $session;
			}, $sessions);
		} catch (Exception $e) {
			$this->logError($e);
			return [];
		}
	}

	/**
	 * Find waiting order by payload
	 * 
	 * @param object $payload Request payload
	 * @return array|null Order data
	 */
	private function findWaitingOrder(object $payload): ?array
	{
		return $this->M_Order->getOne([
			'outlet_id' => $payload->outletId,
			'table_id' => $payload->tableId,
			'brand' => $payload->brand,
			'status' => $this->M_Order::STATUS_WAITING_APPROVAL,
			'deleted_at' => NULL
		]);
	}

	/**
	 * Get data based on action
	 */
	public function getData()
	{
		try {
			$action = $this->input->get('action');
			$this->logDebug('GetData Action: ' . $action);

			if (!method_exists($this, $action)) {
				throw new Exception("Method $action not found");
			}

			$data = $this->$action();
			return $this->sendSuccessResponse('Data retrieved successfully', $data);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}
	/**
	 * List products
	 */
	public function list()
	{
		$tableId = $this->input->get('tableId');
		$outletId = $this->input->get('outletId');
		$category = $this->input->get('category');
		$brandType = $this->input->get('brand') ?? self::DEFAULT_BRAND;
		$isAjax = $this->input->is_ajax_request() || $this->input->get('ajax') === 'true';

		if (empty($outletId) || empty($tableId)) {
			return $isAjax ?
				$this->sendErrorResponse('Parameter tidak lengkap', 400) :
				redirect('/');
		}

		$categories = $this->M_categories->get_catalogue_categories($brandType);
		$products = $this->getProductsByBrand($brandType, $category);

		if ($isAjax) {
			return $this->sendSuccessResponse('Success', [
				'products' => $products,
				'categories' => $categories,
				'grouped_products' => $this->groupProductsByCategory($products)
			]);
		}

		$outlet = $this->MS_Outlet->get_detail_outlet([
			"outlet_id" => $outletId
		]);

		$this->tsmarty->assign("outlet", $outlet);
		$this->tsmarty->assign('template_content', 'order/order.html');
		$this->tsmarty->assign('product_mb', $products);
		$this->tsmarty->assign('grouped_products', $this->groupProductsByCategory($products));
		$this->tsmarty->assign('catalogueCategories', $categories);

		parent::display();
	}

	private function groupProductsByCategory($products)
	{
		$grouped = [];
		foreach ($products as $product) {
			if (!isset($grouped[$product['cat_id']])) {
				$grouped[$product['cat_id']] = [
					'category_name' => $product['cat_name'],
					'products' => []
				];
			}
			$grouped[$product['cat_id']]['products'][] = $product;
		}
		return $grouped;
	}

	/**
	 * Get cart contents
	 */
	public function cart()
	{
		try {
			$params = $this->getAndValidateInputParams();
			$this->logDebug('Cart Params: ' . $this->formatParams($params));

			$order = $this->findActiveOrder($params);
			$this->logDebug('Active Order: ' . print_r($order, true));

			if (!$order) {
				return $this->sendSuccessResponse('No active order', []);
			}

			// Get cart items
			$cartItems = $this->M_Order->getCartItems($order->id);

			// Log cart items
			$this->logDebug('Cart Items: ' . json_encode($cartItems));

			return $this->sendSuccessResponse('Cart retrieved successfully', $cartItems);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Get cart count
	 */
	public function countCart()
	{
		try {
			$params = $this->getAndValidateInputParams();
			$order = $this->findActiveOrder($params);

			if (!$order) {
				return $this->sendSuccessResponse('No active order', 0);
			}

			$count = $this->getCartItemCount($order->id);

			return $this->sendSuccessResponse('Data retrieved', $count);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Add item to cart
	 */
	public function add()
	{
		try {
			// Log raw payload untuk debugging
			$rawPayload = $this->input->raw_input_stream;
			$this->logError('Raw Payload: ' . $rawPayload);

			$payload = $this->security->xss_clean($this->input->raw_input_stream);
			$payload = json_decode($payload);

			// Debug logging
			$this->logError('Decoded Payload: ' . print_r($payload, true));

			// Validasi payload
			if (!$payload) {
				throw new Exception('Invalid payload');
			}

			$this->logDebug('Add to Cart/Update Quantity Payload: ' . print_r($payload, true));

			$this->validateAddToCartPayload($payload);

			$order = $this->findOrCreateOrder($payload);

			// Debug logging tambahan
			$this->logError('Order Details: ' . print_r($order, true));

			switch ($payload->action) {
				case 'addProduct':
					return $this->handleAddProduct($order, $payload);

				case 'increase':
					return $this->handleIncreaseQuantity($order, $payload);

				case 'decrease':
					return $this->handleDecreaseQuantity($order, $payload);

				default:
					$this->logError('Invalid Action: ' . $payload->action);
					throw new Exception('Action not found: ' . $payload->action);
			}
		} catch (Exception $e) {
			$this->logError('Add Method Error: ' . $e->getMessage());
			$this->logError('Error Trace: ' . $e->getTraceAsString());
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	private function handleIncreaseQuantity(object $order, object $payload)
	{
		try {
			$product = $this->MOC_Product->detail($payload->productId);

			// Cari item di order detail
			$orderDetail = $this->db->where([
				'order_id' => $order->id,
				'product_id' => $payload->productId
			])->get('order_details')->row();

			// Validasi stok
			if (!$orderDetail) {
				throw new Exception('Produk tidak ada di keranjang');
			}

			if ($orderDetail->quantity + 1 > $product['stock']) {
				throw new Exception('Stok produk tidak mencukupi');
			}

			// Update quantity di order detail
			$this->db->where([
				'order_id' => $order->id,
				'product_id' => $payload->productId
			])->set('quantity', 'quantity + 1', FALSE)->update('order_details');

			// Kurangi stok produk
			$this->db->where('product_id', $payload->productId)
				->set('stock', 'stock - 1', FALSE)
				->update('data_product');

			return $this->sendSuccessResponse('Kuantitas berhasil diperbarui');
		} catch (Exception $e) {
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	private function handleDecreaseQuantity(object $order, object $payload)
	{
		try {
			// Cari item di order detail
			$orderDetail = $this->db->where([
				'order_id' => $order->id,
				'product_id' => $payload->productId
			])->get('order_details')->row();

			// Validasi item ada di keranjang
			if (!$orderDetail) {
				throw new Exception('Produk tidak ada di keranjang');
			}

			// Jika quantity 1, hapus dari keranjang
			if ($orderDetail->quantity <= 1) {
				$this->db->where([
					'order_id' => $order->id,
					'product_id' => $payload->productId
				])->delete('order_details');

				// Kembalikan stok
				$this->db->where('product_id', $payload->productId)
					->set('stock', 'stock + 1', FALSE)
					->update('data_product');

				return $this->sendSuccessResponse('Produk dihapus dari keranjang');
			}

			// Kurangi quantity
			$this->db->where([
				'order_id' => $order->id,
				'product_id' => $payload->productId
			])->set('quantity', 'quantity - 1', FALSE)->update('order_details');

			// Kembalikan stok
			$this->db->where('product_id', $payload->productId)
				->set('stock', 'stock + 1', FALSE)
				->update('data_product');

			return $this->sendSuccessResponse('Kuantitas berhasil diperbarui');
		} catch (Exception $e) {
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Remove item from cart
	 */
	public function removeCartItem()
	{
		try {
			$payload = $this->getValidatedPayload();

			// Temukan order aktif
			$order = $this->findOrderByIdentity($payload);

			// Hapus produk dari keranjang
			$result = $this->M_Order->removeProductFromCart(
				$order['id'],
				$payload->productId
			);

			if (!$result) {
				throw new Exception('Failed to remove product from cart');
			}

			return $this->sendSuccessResponse('Produk berhasil dihapus dari keranjang');
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	/**
	 * Complete order
	 */
	public function doneOrder()
	{
		try {
			$payload = $this->getValidatedPayload();
			$this->logDebug('Done Order Payload: ' . json_encode($payload));

			$order = $this->M_Order->getOne([
				'outlet_id' => $payload->outletId,
				'table_id' => $payload->tableId,
				'brand' => $payload->brand
			]);

			if (!$order) {
				return $this->sendErrorResponse('Order not found!', 404);
			}

			$orderDetails = $this->MOC_Order_Detail->getAll(['order_id' => $order['id']]);

			if (empty($orderDetails)) {
				return $this->sendErrorResponse('Keranjang kosong!', 400);
			}

			$this->db->trans_begin();

			try {
				// Update product stocks
				$updatedProducts = $this->processOrderProducts($order, $orderDetails);
				if (isset($updatedProducts['error'])) {
					throw new Exception($updatedProducts['message']);
				}
				$this->MOC_Product->updateAll('product_id', $updatedProducts);

				// Update order status
				$this->M_Order->qBUpdate($order['id'], [
					'status' => $this->M_Order::STATUS_ORDERED,
					'updated_at' => $this->getCurrentDateTime()
				]);

				// Reset table status
				$this->resetTableStatus($payload->outletId, $payload->tableId, $payload->brand);

				$this->db->trans_commit();
				return $this->sendSuccessResponse('Order completed successfully');
			} catch (Exception $e) {
				$this->db->trans_rollback();
				throw $e;
			}
		} catch (Exception $e) {
			$this->logError('Done Order Error: ' . $e->getMessage());
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	private function resetTableStatus(int $outletId, int $tableId, string $brand): void
	{
		try {
			// Soft delete approach
			$this->db->where([
				'outlet_id' => $outletId,
				'table_id' => $tableId,
				'brand' => $brand,
				'status' => $this->M_Order::STATUS_ORDERED
			])
				->set('deleted_at', date('Y-m-d H:i:s'))
				->update(self::TABLE);

			$this->logDebug("Table status reset for outlet: $outletId, table: $tableId, brand: $brand");
		} catch (Exception $e) {
			$this->logError("Reset Table Status Error: " . $e->getMessage());
			throw $e; // Re-throw to be handled by caller
		}
	}

	private function getOrderDetails()
	{
		$tableId = $this->input->get('tableId');
		$outletId = $this->input->get('outletId');
		$brand = $this->input->get('brand');

		$order = $this->M_Order->getOne([
			'outlet_id' => $outletId,
			'table_id' => $tableId,
			'brand' => $brand
		]);

		if (!$order) {
			throw new Exception('Pesanan tidak ditemukan');
		}

		$orderDetails = $this->MOC_Order_Detail->getAll(['order_id' => $order['id']]);

		return $orderDetails;
	}

	public function printReceipt()
	{
		$tableId = $this->input->get('tableId');
		$outletId = $this->input->get('outletId');
		$brand = $this->input->get('brand');

		$params = [
			'outlet_id' => $outletId,
			'table_id' => $tableId,
			'brand' => $brand
		];

		$orderData = $this->getOrderData($params);
		$this->tsmarty->assign('data', $orderData);
		$this->tsmarty->display('order/cashier/orderReceipt.html');
	}

	/**
	 * Get and validate input parameters
	 * 
	 * @return array Validated parameters
	 * @throws Exception If parameters are invalid
	 */
	private function getAndValidateInputParams(): array
	{
		$params = [
			'outlet_id' => $this->input->get('outletId'),
			'table_id' => $this->input->get('tableId'),
			'brand' => $this->input->get('brand')
		];

		if (empty($params['outlet_id']) || empty($params['table_id']) || empty($params['brand'])) {
			throw new Exception('Parameter tidak lengkap');
		}

		return $params;
	}

	/**
	 * Format parameters for logging
	 * 
	 * @param array $params Parameters to format
	 * @return string Formatted string
	 */
	private function formatParams(array $params): string
	{
		return implode(', ', array_map(
			function ($key, $value) {
				return "$key: $value";
			},
			array_keys($params),
			$params
		));
	}

	/**
	 * Get validated payload from request
	 * 
	 * @return object Validated payload
	 */
	private function getValidatedPayload(): object
	{
		$payload = $this->security->xss_clean($this->input->raw_input_stream);
		return json_decode($payload);
	}

	/**
	 * Prepare new session data
	 * 
	 * @param object $payload Request payload
	 * @return array Session data
	 */
	private function prepareNewSessionData(object $payload): array
	{
		return [
			'outlet_id' => $payload->outletId,
			'table_id' => $payload->tableId,
			'brand' => $payload->brand,
			'name' => $payload->name,
			'status' => $this->M_Order::STATUS_RESERVED,
			'created_at' => $this->getCurrentDateTime(),
			'updated_at' => $this->getCurrentDateTime(),
			'expire_at' => $this->getDefaultExpiryTime()
		];
	}

	/**
	 * Get default expiry time
	 * 
	 * @return string Expiry datetime
	 */
	private function getDefaultExpiryTime(): string
	{
		return date('Y-m-d H:i:s', strtotime('+' . self::DEFAULT_SESSION_EXPIRY . ' minutes'));
	}

	/**
	 * Get extended expiry time
	 * 
	 * @return string Expiry datetime
	 */
	private function getExtendedExpiryTime(): string
	{
		return date('Y-m-d H:i:s', strtotime('+' . self::EXTENDED_SESSION_EXPIRY . ' minutes'));
	}

	/**
	 * Get current datetime
	 * 
	 * @return string Current datetime
	 */
	private function getCurrentDateTime(): string
	{
		return date('Y-m-d H:i:s');
	}

	/**
	 * Format product prices and group products
	 * 
	 * @param string $brandType Brand type
	 * @param string|null $category Category
	 * @return array Products list
	 */
	private function getProductsByBrand(string $brandType, ?string $category): array
	{
		// Get products based on brand type and category
		if ($category) {
			$products = $this->M_products->get_catalogues(
				'catalogue_outlet_filter_category',
				[$category, $brandType]
			);
		} else {
			$products = $this->M_products->get_catalogues(
				'get_list_product_outlet',
				[$brandType]
			);
		}

		// Format prices and create grouped array
		$grouped = [];
		foreach ($products as &$product) {
			// Format price
			$price = floor($product['price_catalogue']);
			$product['price_catalogue'] = ($price != $product['price_catalogue'])
				? str_replace('.', ',', strval($product['price_catalogue']))
				: $price;

			// Group by category
			if (!isset($grouped[$product['cat_id']])) {
				$grouped[$product['cat_id']] = [
					'category_name' => $product['cat_name'],
					'products' => []
				];
			}

			// Add to group
			$grouped[$product['cat_id']]['products'][] = [
				'product_id' => $product['product_id'],
				'product_name' => $product['product_name'],
				'product_pict' => $product['product_pict'],
				'price_catalogue' => $product['price_catalogue'],
				'stock' => $product['stock'],
				'cat_id' => $product['cat_id']
			];
		}

		// Pass grouped data to view
		$this->tsmarty->assign('grouped_products', $grouped);
		$this->tsmarty->assign('product_mb', $products);

		return $products;
	}

	/**
	 * Format product prices
	 * 
	 * @param array $products Products list
	 */
	private function formatProductPrices(array &$products): void
	{
		foreach ($products as &$product) {
			$temp = floor($product['price_catalogue']);
			$product['price_catalogue'] = ($temp != $product['price_catalogue'])
				? str_replace('.', ',', strval($product['price_catalogue']))
				: $temp;
		}
	}

	/**
	 * Find active order
	 * 
	 * @param array $params Search parameters
	 * @return object|null Order object
	 */
	private function findActiveOrder(array $params): ?object
	{
		return $this->db->select('*')
			->from('orders')
			->where([
				'outlet_id' => $params['outlet_id'],
				'brand' => $params['brand'],
				'table_id' => $params['table_id'],
				'status' => 0,
				'deleted_at IS NULL' => null
			])
			->where('expire_at >', date('Y-m-d H:i:s'))
			->get()
			->row();
	}

	/**
	 * Get cart items
	 * 
	 * @param int $orderId Order ID
	 * @param array $fields Available fields
	 * @return array Cart items
	 */
	private function getCartItems(int $orderId, array $fields): array
	{
		$select = 'od.product_id, dp.product_name, dp.product_pict, 
                  dp.stock AS product_stock, od.quantity AS product_count'
			. (in_array('notes', $fields) ? ', od.notes' : '');

		return $this->db->select($select)
			->from('order_details od')
			->join('data_product dp', 'od.product_id = dp.product_id')
			->where([
				'od.order_id' => $orderId,
				'od.deleted_at IS NULL' => null
			])
			->get()
			->result_array();
	}

	/**
	 * Get cart item count
	 * 
	 * @param int $orderId Order ID
	 * @return int Item count
	 */
	private function getCartItemCount(int $orderId): int
	{
		return $this->db->select('COALESCE(SUM(quantity), 0) as total')
			->from('order_details')
			->where([
				'order_id' => $orderId,
				'deleted_at IS NULL' => null
			])
			->get()
			->row()
			->total;
	}

	/**
	 * Validate add to cart payload
	 * 
	 * @param object $payload Request payload
	 * @throws Exception If payload is invalid
	 */
	private function validateAddToCartPayload(object $payload): void
	{
		// Debug logging
		$this->logError('Validating Payload: ' . print_r($payload, true));

		if (!isset($payload->action)) {
			throw new Exception('Action not specified');
		}

		if (
			!isset($payload->outletId) ||
			!isset($payload->tableId) ||
			!isset($payload->brand) ||
			!isset($payload->productId)
		) {
			throw new Exception('Incomplete parameters');
		}
	}

	/**
	 * Find or create order
	 * 
	 * @param object $payload Request payload
	 * @return object Order data
	 */
	private function findOrCreateOrder(object $payload): object
	{
		$order = $this->db->select('*')
			->from('orders')
			->where([
				'outlet_id' => $payload->outletId,
				'table_id' => $payload->tableId,
				'brand' => $payload->brand
			])
			->order_by('id', 'DESC')
			->limit(1)
			->get()
			->row();

		if (!$order || $order->status == 1) {
			$newOrderData = [
				'outlet_id' => $payload->outletId,
				'table_id' => $payload->tableId,
				'brand' => $payload->brand,
				'name' => $order ? $order->name : 'Unknown',
				'status' => 0,
				'created_at' => $this->getCurrentDateTime(),
				'updated_at' => $this->getCurrentDateTime(),
				'expire_at' => date('Y-m-d H:i:s', strtotime('+1 minute'))
			];

			$this->db->insert('orders', $newOrderData);
			$orderId = $this->db->insert_id();
			$this->logDebug('New Order Created: ' . $orderId);

			$order = (object)array_merge($newOrderData, ['id' => $orderId]);
		}

		return $order;
	}

	/**
	 * Handle add product to cart
	 * 
	 * @param object $order Order data
	 * @param object $payload Request payload
	 * @return CI_Output Response
	 */
	private function handleAddProduct(object $order, object $payload)
	{
		try {
			$product = $this->MOC_Product->detail($payload->productId);

			// Validasi stok sebelum menambah
			$productInCart = $this->M_Order->queryDB('getProductInCart', [
				$order->id,
				$payload->productId
			]);

			$currentQty = $productInCart ? $productInCart[0]->quantity : 0;

			// Tambahkan pengecekan stok yang lebih ketat
			if ($currentQty + 1 > $product['stock']) {
				return $this->sendErrorResponse("Stok produk tidak mencukupi");
			}

			$addResult = $productInCart
				? $this->updateProductQuantity($order->id, $payload->productId, $currentQty)
				: $this->addNewProductToCart($order->id, $payload->productId);

			return $this->sendSuccessResponse('Produk berhasil ditambahkan ke keranjang');
		} catch (Exception $e) {
			return $this->sendErrorResponse($e->getMessage());
		}
	}
	/**
	 * Update product quantity in cart
	 * 
	 * @param int $orderId Order ID
	 * @param int $productId Product ID
	 * @param int $currentQty Current quantity
	 */
	private function updateProductQuantity(int $orderId, int $productId, int $currentQty)
	{
		$updateResult = $this->M_Order->updateOrder('updateOrderDetail', [
			$currentQty + 1,
			$orderId,
			$productId
		]);

		// Kurangi stok produk
		$this->db->set('stock', 'stock - 1', FALSE)
			->where('product_id', $productId)
			->update('data_product');

		return $updateResult;
	}

	/**
	 * Add new product to cart
	 * 
	 * @param int $orderId Order ID
	 * @param int $productId Product ID
	 */
	private function addNewProductToCart(int $orderId, int $productId)
	{
		return $this->M_Order->addToCart([
			'order_id' => $orderId,
			'product_id' => $productId,
			'quantity' => 1,
			'created_at' => $this->getCurrentDateTime()
		]);
	}

	/**
	 * Find order by identity
	 * 
	 * @param object $payload Request payload
	 * @return array Order data
	 */
	private function findOrderByIdentity(object $payload): ?array
	{
		$order = $this->M_Order->getOne([
			'outlet_id' => $payload->outletId,
			'table_id' => $payload->tableId,
			'brand' => $payload->brand,
			'status' => 0, // Hanya untuk order aktif
			'deleted_at' => NULL
		]);

		if (!$order) {
			throw new Exception('Order not found');
		}

		return $order;
	}

	/**
	 * Update cart item quantity
	 * 
	 * @param object $order Order data
	 * @param object $payload Request payload
	 */
	private function updateCartItemQuantity(object $order, object $payload): void
	{
		$productInCart = $this->M_Order->queryDB('getProductInCart', [
			$order->id,
			$payload->productId
		]);

		$newCount = $productInCart[0]->qty - $payload->count;
		$data = ['qty' => $newCount];

		if ($newCount === 0) {
			$data['notes'] = '';
		}

		$this->M_Order->qBOrderDetailUpdate($order->id, $payload->productId, $data);
	}

	/**
	 * Update order expiry time
	 * 
	 * @param object $order Order data
	 */
	private function updateOrderExpiry(object $order): void
	{
		$expiredAt = new DateTime($order->expire_at);
		$currentAt = new DateTime();
		$timeAdd = self::DEFAULT_SESSION_EXPIRY - $expiredAt->diff($currentAt)->format('%i');

		$newExpiredAt = $expiredAt->add(new DateInterval("PT{$timeAdd}M"));

		$this->M_Order->qBUpdate($order->id, [
			'expire_at' => $newExpiredAt->format('Y-m-d H:i:s'),
			'updated_at' => $currentAt->format('Y-m-d H:i:s')
		]);
	}

	/**
	 * Process order products
	 * 
	 * @param array $order Order data
	 * @param array $orderDetails Order details
	 * @return array|array[] Updated products or error
	 */
	private function processOrderProducts(array $order, array $orderDetails)
	{
		$updatedProducts = [];

		foreach ($orderDetails as $detail) {
			// Ambil data produk
			$product = $this->MOC_Product->detail($detail['product_id']);

			$quantity = (int)$detail['quantity']; // Quantity dari order detail
			$newStock = $product['stock'] - $quantity;

			if ($newStock < 0) {
				$this->MOC_Order_Detail->update([
					'order_id' => $order['id'],
					'product_id' => $detail['product_id']
				], [
					'quantity' => $detail['quantity'] + $newStock
				]);

				return [
					'error' => true,
					'message' => "Silahkan periksa kembali. Stock untuk {$product['product_name']} tidak mencukupi."
				];
			}

			$updatedProducts[] = [
				'product_id' => $detail['product_id'],
				'stock' => $newStock
			];
		}

		return $updatedProducts;
	}

	/**
	 * Send success response
	 * 
	 * @param string $message Success message
	 * @param mixed $data Response data
	 * @return CI_Output Response
	 */
	private function sendSuccessResponse(string $message, $data = null)
	{
		$response = [
			'success' => true,
			'message' => $message
		];

		if ($data !== null) {
			$response['data'] = $data;
		}

		return $this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	/**
	 * Send error response
	 * 
	 * @param string $message Error message
	 * @param int $statusCode HTTP status code
	 * @return CI_Output Response
	 */
	private function sendErrorResponse(string $message, int $statusCode = 500)
	{
		return $this->output
			->set_status_header($statusCode)
			->set_content_type('application/json')
			->set_output(json_encode([
				'success' => false,
				'message' => $message
			]));
	}

	/**
	 * Log debug message
	 * 
	 * @param string $message Debug message
	 */
	private function logDebug(string $message): void
	{
		log_message('error', $message);
	}

	/**
	 * Log error message and stack trace
	 * 
	 * @param Exception $e Exception object
	 */

	private function logError($error): void
	{
		if ($error instanceof Exception) {
			log_message('error', $error->getMessage());
			log_message('error', 'Error Trace: ' . $error->getTraceAsString());
		} else {
			log_message('error', (string) $error);
		}
	}
}
