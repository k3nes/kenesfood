<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once(APPPATH . 'controllers/base/PrivateBase.php');

/**
 * Order Controller Class
 * 
 * Handles all order-related operations and views
 */
class Order extends ApplicationBase
{
    /**
     * @var int Number of items per page for pagination
     */
    private const ITEMS_PER_PAGE = 10;

    /**
     * Constructor to load required models
     */
    public function __construct()
    {
        parent::__construct();
        $this->loadRequiredModels();
    }

    /**
     * Load all required models
     */
    private function loadRequiredModels(): void
    {
        $this->load->model('order/M_Order', 'M_Order');
        $this->load->model('settings/M_outlets', 'm_outlets');
        $this->load->model('order/cashier/M_Order', 'M_Order_Cashier');
        $this->load->model('order/cashier/M_Order_Detail', 'M_Order_Detail');
    }

    /**
     * Display main order index page
     */
    public function index(): void
    {
        $this->tsmarty->assign('template_content', 'order/cashier/orderIndex.html');

        $keyword = $this->handleSearch();
        $this->tsmarty->assign('keyword', $keyword);

        $pagination = $this->setupPagination($keyword);
        $this->assignPaginationData($pagination);

        $data = $this->getListData($pagination['start'], $keyword);
        $this->tsmarty->assign('datas', $data);

        parent::display();
    }

    /**
     * Handle search functionality
     * 
     * @return string Search keyword
     */
    private function handleSearch(): string
    {
        $keyword = '';
        $search = $this->session->userdata('search_outlet');

        if ($this->input->post()) {
            if ($this->input->post('save') == 'Reset') {
                $this->session->unset_userdata('search_outlet');
            } else {
                $keyword = $this->input->post('keyword');
                $this->session->set_userdata('search_outlet', ['keyword' => $keyword]);
            }
        } elseif (!empty($search)) {
            $keyword = $search['keyword'];
        }

        return $keyword;
    }

    /**
     * Setup pagination configuration
     * 
     * @param string $keyword Search keyword
     * @return array Pagination configuration
     */
    private function setupPagination(string $keyword): array
    {
        $this->load->library('pagination');

        $config = [
            'base_url' => site_url('master/orders/index/'),
            'total_rows' => $this->m_outlets->get_total_data($keyword),
            'uri_segment' => 4,
            'per_page' => self::ITEMS_PER_PAGE
        ];

        $this->pagination->initialize($config);

        $start = $this->uri->segment(4, 0) + 1;
        $end = min($start + self::ITEMS_PER_PAGE - 1, $config['total_rows']);

        return [
            'data' => $this->pagination->create_links(),
            'start' => ($config['total_rows'] == 0) ? 0 : $start,
            'end' => $end,
            'total' => $config['total_rows']
        ];
    }

    /**
     * Assign pagination data to template
     * 
     * @param array $pagination Pagination data
     */
    private function assignPaginationData(array $pagination): void
    {
        $this->tsmarty->assign('pagination', $pagination);
        $this->tsmarty->assign('no', $pagination['start']);
    }

    /**
     * Get list data with pagination
     * 
     * @param int $start Start index
     * @param string $keyword Search keyword
     * @return array List data
     */
    private function getListData(int $start, string $keyword): array
    {
        $params = [($start - 1), self::ITEMS_PER_PAGE];
        return $this->m_outlets->get_list_data($params, $keyword);
    }

    /**
     * Display order detail page
     * 
     * @param int $id Order ID
     */
    public function detail(int $id): void
    {
        $data = $this->m_outlets->get_detail_outlet($id);

        $this->tsmarty->assign('template_content', 'order/cashier/orderDetail.html');
        $this->tsmarty->assign('datas', $data);

        parent::display();
    }

    /**
     * Get waiting approval sessions
     * 
     * @return array List of waiting sessions
     */
    private function getWaitingApprovalSessions(): array
    {
        try {
            $outletId = $this->input->get('outletId');
            $brand = $this->input->get('brand');

            $sessions = $this->db->select('*')
                ->from('orders')
                ->where([
                    'outlet_id' => $outletId,
                    'brand' => $brand,
                    'deleted_at' => NULL,
                    'status' => $this->M_Order::STATUS_RESERVED
                ])
                ->where('TIMESTAMPDIFF(MINUTE, created_at, NOW()) <', 1)
                ->where('expire_at >', date('Y-m-d H:i:s'))
                ->get()
                ->result_array();

            return array_map(function ($session) {
                $session['is_new'] = true;
                return $session;
            }, $sessions);
        } catch (Exception $e) {
            $this->logError('Get Waiting Sessions Error: ' . $e->getMessage());
            return [];
        }
    }

    /**
     * Get table status
     * 
     * @return array Table status array
     */
    private function getStatusTable(): array
    {
        try {
            $params = [
                'outlet_id' => $this->input->get('outletId'),
                'brand' => $this->input->get('brand')
            ];

            $outlet = $this->m_outlets->get_detail_outlet($params['outlet_id']);
            $table = array_fill(0, $outlet['count_table'], 0);

            $orders = $this->M_Order->getOrderStatus($params);

            foreach ($orders as $order) {
                $tableIndex = $order['table_id'] - 1;

                if ($this->isValidTableIndex($tableIndex, $table)) {
                    $table[$tableIndex] = $this->getTableStatus($order['status']);
                }
            }

            return $table;
        } catch (Exception $e) {
            $this->logError('GetStatusTable Error: ' . $e->getMessage());
            return [];
        }
    }

    /**
     * Check if table index is valid
     * 
     * @param int $index Table index
     * @param array $table Table array
     * @return bool Is valid
     */
    private function isValidTableIndex(int $index, array $table): bool
    {
        return $index >= 0 && $index < count($table);
    }

    /**
     * Get table status based on order status
     * 
     * @param int $orderStatus Order status
     * @return int Table status
     */
    private function getTableStatus(int $orderStatus): int
    {
        if ($orderStatus == $this->M_Order::STATUS_WAITING_APPROVAL) {
            return 2; // Waiting approval
        } elseif ($orderStatus == $this->M_Order::STATUS_RESERVED) {
            return 1; // Reserved
        }
        return 0;
    }

    /**
     * Get data based on action
     */
    public function getData()
    {
        try {
            $action = $this->input->get('action');
            $this->logError('GetData Action: ' . $action);

            if (!method_exists($this, $action)) {
                throw new Exception("Method $action not found");
            }

            $data = $this->$action();
            return $this->sendSuccessResponse('Data retrieved successfully', $data);
        } catch (Exception $e) {
            $this->logError('GetData Error: ' . $e->getMessage());
            $this->logError('Error Trace: ' . $e->getTraceAsString());
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * Download action handler
     * 
     * @return mixed Download data
     */
    public function download()
    {
        $action = $this->input->get('action');
        return $this->$action();
    }

    /**
     * Print receipt
     */
    private function printReceipt(): void
    {
        $params = [
            'outlet_id' => $this->input->get('outletId'),
            'brand' => $this->input->get('brand'),
            'table_id' => $this->input->get('tableId')
        ];

        $orderData = $this->getOrderData($params);
        $this->tsmarty->assign('data', $orderData);
        $this->tsmarty->display('order/cashier/orderReceipt.html');
    }

    /**
     * Get order data for receipt
     * 
     * @param array $params Query parameters
     * @return array Order data
     */
    private function getOrderData(array $params): array
    {
        $order = $this->M_Order->getOrderWithDetail($params);
        $outlet = $this->m_outlets->get_detail_outlet($params['outlet_id']);
        $customer = $this->M_Order->getOne(array_merge($params, [
            'deleted_at' => NULL,
            'status' => 1,
        ]));

        $orderDateTime = new DateTime($customer['created_at']);

        return [
            'order' => $order,
            'outlet' => $outlet,
            'customer' => [
                'order' => $customer,
                'date' => $orderDateTime->format('d/m/Y'),
                'time' => $orderDateTime->format('H:i')
            ]
        ];
    }

    /**
     * Get order details
     * 
     * @return array Order details
     */
    private function getOrder(): array
    {
        $params = [
            'table_id' => $this->input->get('tableId'),
            'outlet_id' => $this->input->get('outletId'),
            'brand' => $this->input->get('brand')
        ];

        $order = $this->M_Order->getOne(array_merge($params, [
            'deleted_at' => NULL,
            'status' => 1,
        ]));

        $orderDetail = $this->M_Order->getDetailsByOrderId($order['id']);

        return [
            'order' => $order,
            'orderDetails' => $orderDetail
        ];
    }

    /**
     * Approve session
     */
    public function approveSession()
{
    try {
        $payload = json_decode($this->input->raw_input_stream);
        $order = $this->findWaitingOrder($payload);

        if (!$order) {
            throw new Exception('Sesi tidak ditemukan');
        }

        $currentTime = date('Y-m-d H:i:s');
        $this->M_Order->qBUpdate($order['id'], [
            'status' => $this->M_Order::STATUS_RESERVED,
            'updated_at' => $currentTime,
            'expire_at' => date('Y-m-d H:i:s', strtotime('+1 hour'))
        ]);

        return $this->sendSuccessResponse('Sesi disetujui', [
            'approved_at' => $currentTime
        ]);
    } catch (Exception $e) {
        $this->logError('Approve Session Error: ' . $e->getMessage());
        return $this->sendErrorResponse($e->getMessage());
    }
}

    /**
     * Reject session
     */
    public function rejectSession()
    {
        try {
            $payload = $this->getValidatedPayload();
            $order = $this->findWaitingOrder($payload);

            if (!$order) {
                throw new Exception('Sesi tidak ditemukan');
            }

            $updateResult = $this->M_Order->updateSessionStatus(
                $order['id'],
                $this->M_Order::STATUS_REJECTED
            );

            if (!$updateResult) {
                throw new Exception('Gagal memperbarui status sesi');
            }

            return $this->sendSuccessResponse('Sesi ditolak');
        } catch (Exception $e) {
            $this->logError('Reject Session Error: ' . $e->getMessage());
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * Clean expired sessions
     */
    public function cleanExpiredSessions()
    {
        try {
            $outletId = $this->input->get('outletId');
            $brand = $this->input->get('brand');

            $this->M_Order->cleanExpiredSessions($outletId, $brand);
            return $this->sendSuccessResponse('Sesi kadaluarsa telah dibersihkan');
        } catch (Exception $e) {
            $this->logError('Clean Expired Sessions Error: ' . $e->getMessage());
            return $this->sendErrorResponse($e->getMessage());
        }
    }

    /**
     * Delete order
     * 
     * @param int $id Order ID
     */
    public function delete(int $id)
    {
        $params = ['deleted_at' => date('Y-m-d H:i:s')];

        $this->M_Order->update($id, array_merge($params, [
            'cashier_id' => $this->user_data['user_id']
        ]));
        $this->M_Order_Detail->updateByOrderId($id, $params);

        return $this->sendSuccessResponse('Data has been deleted!');
    }

    /**
     * Find waiting order by payload
     * 
     * @param object $payload Request payload
     * @return array|null Order data
     */
    private function findWaitingOrder(object $payload): ?array
    {
        return $this->M_Order->getOne([
            'outlet_id' => $payload->outletId,
            'table_id' => $payload->tableId,
            'brand' => $payload->brand,
            'status' => $this->M_Order::STATUS_WAITING_APPROVAL,
            'deleted_at' => NULL
        ]);
    }

    /**
     * Get validated payload from request
     * 
     * @return object Validated payload
     * @throws Exception If payload is invalid
     */
    private function getValidatedPayload(): object
    {
        $payload = $this->security->xss_clean($this->input->raw_input_stream);
        $payload = json_decode($payload);

        if (!$this->isValidPayload($payload)) {
            throw new Exception('Invalid payload');
        }

        return $payload;
    }

    /**
     * Check if payload is valid
     * 
     * @param object|null $payload Request payload
     * @return bool Is valid
     */
    private function isValidPayload(?object $payload): bool
    {
        return $payload
            && isset($payload->outletId)
            && isset($payload->tableId)
            && isset($payload->brand);
    }

    /**
     * Send success response
     * 
     * @param string $message Success message
     * @param mixed $data Response data
     * @return CI_Output Response output
     */
    private function sendSuccessResponse(string $message, $data = null)
    {
        $response = [
            'success' => true,
            'message' => $message
        ];

        if ($data !== null) {
            $response['data'] = $data;
        }

        return $this->output->set_output(json_encode($response));
    }

    /**
     * Send error response
     * 
     * @param string $message Error message
     * @param int $statusCode HTTP status code
     * @return CI_Output Response output
     */
    private function sendErrorResponse(string $message, int $statusCode = 500)
    {
        return $this->output
            ->set_status_header($statusCode)
            ->set_output(json_encode([
                'success' => false,
                'message' => $message
            ]));
    }

    /**
     * Log error message
     * 
     * @param string $message Error message
     */
    private function logError(string $message): void
    {
        log_message('error', $message);
    }
}
