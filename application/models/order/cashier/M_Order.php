<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Order Model Class
 * 
 * Handles all database operations related to orders
 */
class M_Order extends CI_Model
{
    /**
     * @var string Table name for orders
     */
    protected $table = 'orders';

    /**
     * @var string Primary key field name
     */
    protected $primaryKey = 'id';

    /**
     * Order status constants
     */
    const STATUS_WAITING_APPROVAL = -1;
    const STATUS_REJECTED = -2;
    const STATUS_RESERVED = 0;
    const STATUS_ORDERED = 1;

    /**
     * @var array Valid order statuses for filtering
     */
    protected $validStatuses = [
        self::STATUS_WAITING_APPROVAL,
        self::STATUS_RESERVED,
        self::STATUS_ORDERED
    ];

    /**
     * Get order status based on outlet and brand
     *
     * @param array $params Parameters containing outlet_id and brand
     * @return array Array of order statuses
     */
    public function getOrderStatus(array $params): array
    {
        $query = $this->db
            ->select('table_id, status')
            ->from($this->table)
            ->where([
                'outlet_id' => $params['outlet_id'],
                'brand' => $params['brand'],
                'deleted_at IS NULL' => null
            ])
            ->where_in('status', $this->validStatuses)
            ->where('expire_at >', date('Y-m-d H:i:s'))
            ->get();

        return ($query->num_rows() > 0) ? $query->result_array() : [];
    }

    /**
     * Get single order record
     *
     * @param array $params Search parameters
     * @return array|null Order data or null if not found
     */
    public function getOne(array $params): ?array
    {
        $this->logDebug('GetOne Params: ' . print_r($params, true));

        unset($params['deleted_at']);

        $query = $this->db
            ->where($params)
            ->where('deleted_at IS NULL')
            ->limit(1)
            ->get($this->table);

        $result = $query->row_array();
        $this->logDebug('GetOne Query Result: ' . print_r($result, true));

        return $result;
    }

    /**
     * Get order with product details
     *
     * @param array $params Search parameters
     * @return array Order details
     */
    public function getOrderWithDetail(array $params): array
    {
        $query = $this->db
            ->select('product_name, qty, notes')
            ->from($this->table)
            ->join('order_details', "{$this->table}.id = order_details.order_id")
            ->join('data_product', 'order_details.product_id = data_product.product_id')
            ->where($params)
            ->where("{$this->table}.deleted_at", null)
            ->get();

        return ($query->num_rows() > 0) ? $query->result_array() : [];
    }

    /**
     * Get order details by order ID
     *
     * @param int $id Order ID
     * @return array Order details
     */
    public function getDetailsByOrderId(int $id): array
    {
        $query = $this->db
            ->from('order_details')
            ->join('data_product', 'order_details.product_id = data_product.product_id')
            ->where('order_id', $id)
            ->get();

        return ($query->num_rows() > 0) ? $query->result_array() : [];
    }

    /**
     * Update order data
     *
     * @param int $id Order ID
     * @param array $data Update data
     * @return array Updated data
     */
    public function update(int $id, array $data): array
    {
        $this->db
            ->where($this->primaryKey, $id)
            ->update($this->table, $data);

        return $data;
    }

    /**
     * Update order session status
     *
     * @param int $orderId Order ID
     * @param int $status New status
     * @return bool Success status
     */
    public function updateSessionStatus(int $orderId, int $status): bool
    {
        $updateData = [
            'status' => $status,
            'updated_at' => $this->getCurrentDateTime()
        ];

        if ($status === self::STATUS_REJECTED) {
            $updateData['deleted_at'] = $this->getCurrentDateTime();
        }

        $this->db
            ->where('id', $orderId)
            ->update($this->table, $updateData);

        return $this->db->affected_rows() > 0;
    }

    /**
     * Clean expired and rejected sessions for specific outlet and brand
     *
     * @param int $outletId Outlet ID
     * @param string $brand Brand name
     * @throws Exception When cleaning fails
     */
    public function cleanExpiredAndRejectedSessions(int $outletId, string $brand): void
    {
        try {
            $now = $this->getCurrentDateTime();

            $this->logDebug("Clean Sessions Params - OutletId: $outletId, Brand: $brand, Now: $now");

            $this->db
                ->where([
                    'outlet_id' => $outletId,
                    'brand' => $brand
                ])
                ->group_start()
                ->where('expire_at <', $now)
                ->or_where('status', self::STATUS_REJECTED)
                ->group_end()
                ->set('deleted_at', $now)
                ->update($this->table);

            $this->logDebug('Cleaned Sessions Rows: ' . $this->db->affected_rows());
        } catch (Exception $e) {
            $this->logDebug('Clean Sessions Error: ' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Clean all expired sessions
     */
    private function cleanExpiredSessions(): void
    {
        $now = $this->getCurrentDateTime();

        $this->db
            ->where('expire_at <', $now)
            ->or_where('status', self::STATUS_REJECTED)
            ->set('deleted_at', $now)
            ->update($this->table);
    }

    /**
     * Get current datetime string
     *
     * @return string Current datetime in Y-m-d H:i:s format
     */
    private function getCurrentDateTime(): string
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * Log debug message
     *
     * @param string $message Debug message
     */
    private function logDebug(string $message): void
    {
        log_message('error', $message);
    }
}
