<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * Order Model Class
 * 
 * Handles all database operations for orders
 */
class M_Order extends CI_Model
{
	/**
	 * @var string Table name
	 */
	protected const TABLE = 'orders';

	/**
	 * Order status constants
	 */
	public const STATUS_WAITING_APPROVAL = -1;
	public const STATUS_REJECTED = -2;
	public const STATUS_RESERVED = 0;
	public const STATUS_ORDERED = 1;

	/**
	 * Update order detail quantity SQL
	 *
	 * @return string SQL query
	 */
	private function updateOrderDetail(): string
	{
		return "UPDATE order_details 
                SET quantity = ? 
                WHERE order_id = ? 
                AND product_id = ?";
	}

	/**
	 * Get product in cart SQL
	 *
	 * @return string SQL query
	 */
	private function getProductInCart(): string
	{
		return "SELECT * 
                FROM order_details AS od 
                WHERE od.order_id = ? 
                AND od.product_id = ? 
                LIMIT 1";
	}

	/**
	 * Add item to cart
	 *
	 * @param array $data Product data
	 * @return array Response
	 */
	public function addToCart(array $data): array
	{
		try {
			$product = $this->db->get_where('data_product', ['product_id' => $data['product_id']])->row_array();

			// Debug logging
			$this->logError('Adding to Cart - Product Data: ' . print_r($product, true));
			$this->logError('Adding to Cart - Cart Data: ' . print_r($data, true));

			// Cek stok produk
			if ($product['stock'] <= 0) {
				$this->logError('Add to Cart Failed: Insufficient Stock');
				return $this->createErrorResponse('Stok produk habis');
			}

			// Kurangi stok
			$this->db->set('stock', 'stock - 1', FALSE)
				->where('product_id', $data['product_id'])
				->update('data_product');

			// Tambah ke order detail
			$query = $this->db->insert('order_details', $data);

			if (!$query) {
				// Kembalikan stok jika gagal
				$this->db->set('stock', 'stock + 1', FALSE)
					->where('product_id', $data['product_id'])
					->update('data_product');

				$error = $this->db->error()['message'];
				$this->logError('Add to Cart DB Error: ' . $error);
				return $this->createErrorResponse($error);
			}

			return $this->createSuccessResponse('Produk berhasil ditambahkan', $data);
		} catch (Exception $e) {
			$this->logError('Add to Cart Exception: ' . $e->getMessage());
			return $this->createErrorResponse($e->getMessage());
		}
	}

	private function findActiveOrder(array $params): ?object
	{
		return $this->db->select('*')  // Ubah dari select('id') ke select('*')
			->from('orders')
			->where([
				'outlet_id' => $params['outlet_id'],
				'brand' => $params['brand'],
				'table_id' => $params['table_id'],
				'status' => 0,
				'deleted_at IS NULL' => null
			])
			->where('expire_at >', date('Y-m-d H:i:s'))
			->get()
			->row();
	}

	/**
	 * Get all orders
	 *
	 * @param array $params Filter parameters
	 * @return array Orders list
	 */
	public function getAll(array $params = []): array
	{
		$query = $this->db->where($params)
			->where('expire_at >', $this->getCurrentDateTime())
			->get(self::TABLE);

		$this->logError('M_Order getAll Query: ' . $this->db->last_query());

		return $query->result_array();
	}

	/**
	 * Get cart SQL
	 *
	 * @return string SQL query
	 */

	public function cart()
	{
		try {
			$params = $this->getAndValidateInputParams();
			$this->logDebug('Cart Params: ' . $this->formatParams($params));

			$order = $this->findActiveOrder($params);
			$this->logDebug('Active Order: ' . print_r($order, true));

			if (!$order) {
				return $this->sendSuccessResponse('No active order', []);
			}

			$fields = $this->db->list_fields('order_details');
			$this->logDebug('Order Details Fields: ' . print_r($fields, true));

			$cartItems = $this->getCartItems($order->id, $fields);
			$this->logDebug('Cart Items: ' . print_r($cartItems, true));

			return $this->sendSuccessResponse('Data has been retrieved', $cartItems);
		} catch (Exception $e) {
			$this->logError($e);
			return $this->sendErrorResponse($e->getMessage());
		}
	}

	public function getCartItems(int $orderId): array
	{
		$select = 'od.product_id, 
               od.quantity,
               dp.product_name, 
               dp.product_pict, 
               dp.stock AS product_stock,
               dp.product_price,
               ROUND(dp.product_price/1000, 1) AS price_catalogue';

		$query = $this->db->select($select)
			->from('order_details od')
			->join('data_product dp', 'od.product_id = dp.product_id')
			->where([
				'od.order_id' => $orderId,
				'od.deleted_at IS NULL' => null
			])
			->get();

		$results = $query->result_array();

		// Log untuk debugging
		log_message('error', 'Cart Query: ' . $this->db->last_query());
		log_message('error', 'Cart Results: ' . json_encode($results));

		return $results;
	}

	private function logCartData($orderId)
	{
		$sql = "SELECT od.*, dp.product_name, dp.product_price 
				FROM order_details od
				LEFT JOIN data_product dp ON od.product_id = dp.product_id
				WHERE od.order_id = ?";
		$query = $this->db->query($sql, [$orderId]);
		$this->logDebug('Cart Data Debug: ' . json_encode($query->result_array()));
	}

	public function getCart(): string
	{
		return "SELECT 
        o.outlet_id,
        o.table_id,
        od.product_id,
        od.notes,
        dp.product_name,
        dp.product_pict,
        dp.stock AS product_stock,
        ROUND(dp.product_price/1000, 1) AS price_catalogue,
        od.quantity AS quantity
    FROM orders AS o
    INNER JOIN order_details AS od ON o.id = od.order_id
    INNER JOIN data_product AS dp ON od.product_id = dp.product_id
    WHERE o.outlet_id = ?
        AND o.brand = ?
        AND o.table_id = ?
        AND o.status = 0
        AND od.deleted_at IS NULL
        AND o.deleted_at IS NULL";
	}

	/**
	 * Get cart by product SQL
	 *
	 * @return string SQL query
	 */
	public function getCartByProduct(): string
	{
		return "SELECT * 
                FROM data_orders 
                WHERE outlet_id = ? 
                    AND brand = ? 
                    AND table_id = ? 
                    AND product_id = ?";
	}

	/**
	 * Get cart count SQL
	 *
	 * @return string SQL query
	 */
	public function getCountCart(): string
	{
		return "SELECT COALESCE(SUM(od.quantity), 0) AS count
                FROM orders AS o
                INNER JOIN order_details AS od ON o.id = od.order_id
                WHERE o.outlet_id = ?
                    AND o.brand = ?
                    AND o.table_id = ?
                    AND o.status = 0
                    AND (od.deleted_at IS NULL OR od.deleted_at = '')
                    AND (o.deleted_at IS NULL OR o.deleted_at = '')";
	}

	/**
	 * Get placed order SQL
	 *
	 * @return string SQL query
	 */
	public function getPlacedOrder(): string
	{
		return "SELECT SUM(od.qty) AS count
                FROM " . self::TABLE . " AS o
                INNER JOIN order_details AS od ON o.id = od.order_id
                WHERE o.outlet_id = ?
                    AND o.table_id = ?
                    AND o.brand = ?
                    AND o.status = 1";
	}

	/**
	 * Remove items from cart
	 *
	 * @param array $data Cart data
	 * @param int $count Items count
	 * @return array Response
	 */
	public function removeCartItem(array $data, int $count): array
	{
		try {
			$orders = $this->queryDB('getCartByProduct', $data);
			$deletion = array_slice($orders, 0, $count);
			$orderCodes = array_map(function ($item) {
				return $item->order_code;
			}, $deletion);

			$this->db->where_in('order_code', $orderCodes)->delete('data_orders');

			return $this->createSuccessResponse('Data was deleted', $data);
		} catch (Exception $e) {
			$this->logError('Remove Cart Item Error: ' . $e->getMessage());
			return $this->createErrorResponse($e->getMessage());
		}
	}

	/**
	 * Update done order SQL
	 *
	 * @return string SQL query
	 */
	public function updateDoneOrder(): string
	{
		return "UPDATE " . self::TABLE . "
                SET status = 1
                WHERE outlet_id = ?
                    AND table_id = ?
                    AND brand = ?";
	}

	/**
	 * Update order
	 *
	 * @param string $action Action method name
	 * @param array $params Query parameters
	 * @return array Response
	 */
	public function updateOrder(string $action, array $params): array
	{
		if (!method_exists($this, $action)) {
			return [];
		}

		try {
			$sql = $this->$action();
			$query = $this->db->query($sql, $params);

			if (!$query) {
				return $this->createErrorResponse($this->db->error());
			}

			return $this->createSuccessResponse('Order has been placed!');
		} catch (Exception $e) {
			$this->logError('Update Order Error: ' . $e->getMessage());
			return $this->createErrorResponse($e->getMessage());
		}
	}

	/**
	 * Get one by identity SQL
	 *
	 * @return string SQL query
	 */
	public function getOneByIdentity(): string
	{
		return "SELECT *
                FROM " . self::TABLE . "
                WHERE outlet_id = ?
                    AND table_id = ?
                    AND brand = ?
                    AND deleted_at IS NULL
                LIMIT 1";
	}

	/**
	 * Delete order by ID
	 *
	 * @param int $id Order ID
	 */
	public function deleteById(int $id): void
	{
		$this->db->trans_begin();

		try {
			$now = $this->getCurrentDateTime();

			// Delete order
			$this->db->where('id', $id)
				->set('deleted_at', $now)
				->update(self::TABLE);

			// Delete order details
			$this->db->where('order_id', $id)
				->set('deleted_at', $now)
				->update('order_details');

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return;
			}

			$this->db->trans_commit();
		} catch (Exception $e) {
			$this->db->trans_rollback();
			$this->logError('Delete Order Error: ' . $e->getMessage());
		}
	}

	/**
	 * Insert new order
	 *
	 * @param array $data Order data
	 * @return array Response
	 */
	public function insertOrder(array $data): array
	{
		$this->db->trans_start();

		try {
			$this->logError('Before Insert Data: ' . print_r($data, true));

			$this->db->insert(self::TABLE, $data);
			$insertId = $this->db->insert_id();

			$inserted = $this->db->get_where(self::TABLE, ['id' => $insertId])->row_array();
			$this->logError('After Insert Data: ' . print_r($inserted, true));

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return $this->createErrorResponse('Failed to insert order');
			}

			$this->db->trans_commit();
			return [
				'success' => true,
				'id' => $insertId,
				'data' => $inserted
			];
		} catch (Exception $e) {
			$this->db->trans_rollback();
			$this->logError('Insert Order Error: ' . $e->getMessage());
			return $this->createErrorResponse($e->getMessage());
		}
	}

	/**
	 * Execute dynamic query
	 *
	 * @param string $action Method name
	 * @param array $params Query parameters
	 * @return array Query results
	 */
	public function queryDB(string $action, array $params): array
	{
		if (!method_exists($this, $action)) {
			return [];
		}

		try {
			$sql = $this->$action();
			$query = $this->db->query($sql, $params);
			return $query->result();
		} catch (Exception $e) {
			$this->logError('Query DB Error: ' . $e->getMessage());
			return [];
		}
	}

	/**
	 * Quick update order
	 *
	 * @param int $id Order ID
	 * @param array $data Update data
	 */
	public function qBUpdate(int $id, array $data): void
	{
		$this->db->where('id', $id)
			->update(self::TABLE, $data);
	}
	/**
	 * Quick update order detail
	 *
	 * @param int $orderId Order ID
	 * @param int $productId Product ID
	 * @param array $data Update data
	 */
	public function qBOrderDetailUpdate(int $orderId, int $productId, array $data): void
	{
		$this->db->where([
			'order_id' => $orderId,
			'product_id' => $productId
		])->update('order_details', $data);
	}

	/**
	 * Get single order
	 *
	 * @param array $params Query parameters
	 * @return array|null Order data
	 */
	public function getOne(array $params): ?array
	{
		$this->logError('GetOne Detailed Params: ' . print_r($params, true));

		unset($params['deleted_at']);

		$query = $this->db
			->where([
				'outlet_id' => $params['outlet_id'],
				'table_id' => $params['table_id'],
				'brand' => $params['brand'],
				'deleted_at IS NULL' => null
			])
			->where('expire_at >', $this->getCurrentDateTime())
			->where_in('status', [0, -1])
			->order_by('id', 'DESC')
			->limit(1)
			->get(self::TABLE);

		$result = $query->row_array();
		$this->logError('GetOne Detailed Query Result: ' . print_r($result, true));

		return $result;
	}

	/**
	 * Get order status
	 *
	 * @param array $params Query parameters
	 * @return array Order status
	 */
	public function getOrderStatus(array $params): array
	{
		$query = $this->db
			->select('table_id, status')
			->from(self::TABLE)
			->where([
				'outlet_id' => $params['outlet_id'],
				'brand' => $params['brand'],
				'deleted_at IS NULL' => null
			])
			->where('expire_at >', $this->getCurrentDateTime())
			->where_in('status', [
				self::STATUS_WAITING_APPROVAL,
				self::STATUS_RESERVED
			])
			->get();

		return $query->result_array();
	}

	/**
	 * Get waiting approval sessions
	 *
	 * @param array $params Query parameters
	 * @return array Waiting sessions
	 */
	public function getWaitingApprovalSessions(array $params): array
	{
		try {
			$query = $this->db
				->where([
					'outlet_id' => $params['outlet_id'],
					'brand' => $params['brand'],
					'status' => self::STATUS_WAITING_APPROVAL,
					'deleted_at' => NULL
				])
				->where('expire_at >', $this->getCurrentDateTime())
				->get(self::TABLE);

			$this->logError('Waiting Sessions Query: ' . $this->db->last_query());
			$result = $query->result_array();
			$this->logError('Found Sessions: ' . json_encode($result));

			return $result;
		} catch (Exception $e) {
			$this->logError('Get Waiting Sessions Error: ' . $e->getMessage());
			return [];
		}
	}

	/**
	 * Update session status
	 *
	 * @param int $orderId Order ID
	 * @param int $status New status
	 * @return bool Success status
	 */
	public function updateSessionStatus(int $orderId, int $status): bool
	{
		$updateData = [
			'status' => $status,
			'updated_at' => $this->getCurrentDateTime()
		];

		if ($status === self::STATUS_REJECTED) {
			$updateData['deleted_at'] = $this->getCurrentDateTime();
		}

		$this->db->where('id', $orderId)
			->update(self::TABLE, $updateData);

		return $this->db->affected_rows() > 0;
	}

	/**
	 * Clean expired and rejected sessions
	 *
	 * @param int $outletId Outlet ID
	 * @param string $brand Brand name
	 */
	public function cleanExpiredAndRejectedSessions(int $outletId, string $brand): void
	{
		$now = $this->getCurrentDateTime();
		$this->logError("Clean Sessions Params - OutletId: $outletId, Brand: $brand, Now: $now");

		$deletedRows = $this->db
			->where([
				'outlet_id' => $outletId,
				'brand' => $brand
			])
			->group_start()
			->where('expire_at <', $now)
			->or_where('status', self::STATUS_REJECTED)
			->group_end()
			->delete(self::TABLE);

		$this->logError("Deleted Expired Sessions: $deletedRows");
	}

	/**
	 * Clean expired sessions
	 */
	private function cleanExpiredSessions(): void
	{
		$this->db->where('expire_at <', $this->getCurrentDateTime())
			->or_where('status', self::STATUS_REJECTED)
			->set('deleted_at', $this->getCurrentDateTime())
			->update(self::TABLE);
	}

	public function removeProductFromCart(int $orderId, int $productId): bool
	{
		$this->db->trans_start();

		// Hapus produk dari order_details
		$this->db->where([
			'order_id' => $orderId,
			'product_id' => $productId
		])->delete('order_details');

		// Update stok produk
		$this->restoreProductStock($productId);

		$result = $this->db->trans_complete();
		return $result !== FALSE;
	}

	private function restoreProductStock(int $productId): void
	{
		$this->db->set('stock', 'stock + 1', FALSE)
			->where('product_id', $productId)
			->update('data_product');
	}

	/**
	 * Create success response
	 *
	 * @param string $message Success message
	 * @param mixed $data Response data
	 * @return array Response array
	 */
	private function createSuccessResponse(string $message, $data = null): array
	{
		$response = [
			'success' => true,
			'message' => $message
		];

		if ($data !== null) {
			$response['data'] = $data;
		}

		return $response;
	}

	/**
	 * Create error response
	 *
	 * @param string|array $error Error message or array
	 * @return array Response array
	 */
	private function createErrorResponse($error): array
	{
		return [
			'success' => false,
			'message' => $error
		];
	}

	/**
	 * Get current datetime
	 *
	 * @return string Current datetime in Y-m-d H:i:s format
	 */
	private function getCurrentDateTime(): string
	{
		return date('Y-m-d H:i:s');
	}

	/**
	 * Log error message
	 *
	 * @param string $message Error message
	 */
	private function logError(string $message): void
	{
		log_message('error', $message);
	}
}
