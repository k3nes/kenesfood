import gulp from 'gulp';
import stripDebug from 'gulp-strip-debug';
import terser from 'gulp-terser';

function cleanJs() {
  return gulp.src('resource/assets-frontend/js/*.js')
    .pipe(stripDebug())
    .pipe(terser({
      compress: {
        drop_console: true
      }
    }))
    .pipe(gulp.dest('resource/assets-frontend/js/production/'));
}

export default cleanJs;